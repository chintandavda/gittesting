from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'gitapp/index.html')

def here(request):
    return render(request, 'gitapp/here.html')